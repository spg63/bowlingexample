import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/*TODO

 */
public class BowlingGameTest {

    private BowlingGame game;

    @BeforeEach
    public void setup() {
        game = new BowlingGame();
    }

    @Test
    public void gutter_game() {
        rollMany(20, 0);
        assertEquals(0, game.score());
    }

    @Test
    public void all_ones() {
        rollMany(20, 1);
        assertEquals(20, game.score());
    }

    @Test
    public void one_spare() {
        rollSpare();
        game.roll(3);
        rollMany(17, 0);
        assertEquals(16, game.score());
    }

    @Test
    public void one_strike() {
        rollStrike();
        game.roll(3);
        game.roll(4);
        rollMany(16, 0);
        assertEquals(24, game.score());
    }

    @Test
    public void perfect_game() {
        rollMany(12, 10);
        assertEquals(300, game.score());
    }

    private void rollMany(int rolls, int pins) {
        for (int i = 0; i < rolls; i++) {
            game.roll(pins);
        }
    }

    private void rollSpare() {
        game.roll(5);
        game.roll(5);
    }

    private void rollStrike() {
        game.roll(10);
    }
}
