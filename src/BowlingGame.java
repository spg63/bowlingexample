import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BowlingGame {
    private List<Integer> rolls = new ArrayList(Collections.nCopies(21, 0));
    private int currentRoll = 0;

    public void roll(int pins) {
        rolls.set(currentRoll, pins);
        ++currentRoll;
    }

    public int score() {
        int score = 0;
        int frameIndex = 0;
        for (int frame = 0; frame < 10; ++frame) {
            if (isStrike(frameIndex)) {
                score += 10 + strikeBonus(frameIndex);
                ++frameIndex;
            } else if (isSpare(frameIndex)) {
                score += 10 + spareBonus(frameIndex);
                frameIndex += 2;
            } else {
                score += rolls.get(frameIndex) + rolls.get(frameIndex + 1);
                frameIndex += 2;
            }
        }

        return score;
    }

    private boolean isSpare(int frameIndex) {
        return rolls.get(frameIndex) + rolls.get(frameIndex + 1) == 10;
    }

    private int spareBonus(int frameIndex) {
        return rolls.get(frameIndex + 2);
    }

    private boolean isStrike(int frameIndex) {
        return rolls.get(frameIndex) == 10;
    }

    private int strikeBonus(int frameIndex) {
        return rolls.get(frameIndex + 1) + rolls.get(frameIndex + 2);
    }
}