import java.util.List;

public class Building {
    private List<Room> rooms;
    private Address address;

    public Building(Address address, List<Room> rooms) {
        this.address = address;
        this.rooms = rooms;
    }

    public List<Room> getRooms() {
        return this.rooms;
    }

    public Address getAddress() {
        return this.address;
    }
}
