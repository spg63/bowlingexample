public class Bowler {
    private String name;
    private int bestScore;

    public Bowler(String name, int bestScore) {
        this.name = name;
        this.bestScore = bestScore;
    }

    public String getName() {
        return this.name;
    }

    public int getBestScore() {
        return this.bestScore;
    }
}
