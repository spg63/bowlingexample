import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Address address = new Address("3675 Market Street");

        Room leftSide = new Room(100, 50);
        Room rightSide = new Room(100, 75);
        Room equipmentArea = new Room(20, 20);
        List<Room> allRooms = new ArrayList<>();
        allRooms.add(leftSide);
        allRooms.add(rightSide);
        allRooms.add(equipmentArea);

        String alleyName = "Drexel Bowling";
        boolean openToPublic = false;

        Bowler okayBowler = new Bowler("John", 140);
        Bowler greatBowler = new Bowler("Abby", 290);
        Bowler terribleBowler = new Bowler("Kurt", 28);
        List<Bowler> allBowlers = new ArrayList<>();
        allBowlers.add(okayBowler);
        allBowlers.add(greatBowler);
        allBowlers.add(terribleBowler);

        BowlingGame game = new BowlingGame();

        BowlingAlley bowling = new BowlingAlley(address, allRooms, alleyName,
                openToPublic, allBowlers, game);

        bowling.displayBowlers();

    }


}
