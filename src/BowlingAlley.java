import java.util.List;

public class BowlingAlley extends Building {
    private String name;
    private boolean openToPublic;
    private List<Bowler> bowlers;
    private BowlingGame game;

    public BowlingAlley(Address address, List<Room> rooms, String name,
                        boolean publicAccess, List<Bowler> bowlers,
                        BowlingGame theGame) {
        super(address, rooms);
        this.name = name;
        this.openToPublic = publicAccess;
        this.bowlers = bowlers;
        this.game = theGame;
    }

    public boolean isOpenToPublic() {
        return openToPublic;
    }

    public String getName() {
        return this.name;
    }

    public void displayBowlers() {
        for (Bowler b : this.bowlers) {
            System.out.println(
                    "Bowler: " + b.getName() + " | Top Score: " + b.getBestScore()
            );
        }
    }
}
